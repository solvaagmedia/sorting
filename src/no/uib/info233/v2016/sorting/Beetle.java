package no.uib.info233.v2016.sorting;

public interface Beetle extends Comparable<Beetle> {
	
	int getWeight();
	
	int getAge();
	
	String getClassification();
	
	String getName(); 
	

}
