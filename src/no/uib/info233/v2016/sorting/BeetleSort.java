package no.uib.info233.v2016.sorting;

import no.uib.info233.v2016.sorting.beetles.AciliusSulcatus;

public class BeetleSort {
	
	public static void main(String[] args) {
		
		Beetle[] beetles = {new AciliusSulcatus(1, 4, "lol", "hepp"),new AciliusSulcatus(1, 3, "lol", "hepp"),
				new AciliusSulcatus(4, 3, "lol", "hepp"),
				new AciliusSulcatus(5, 6, "lol", "hepp"),
				new AciliusSulcatus(3, 1, "lol", "hepp"),
				new AciliusSulcatus(1, 7, "lol", "hepp")} ;
		beetleSort(beetles);
		
		for (int i = 0; i < beetles.length; i++) {
			System.out.println(beetles[i].getWeight());
		}
	}
	
	public static void beetleSort(Beetle[] beetles) {
		
		Beetle swap;
		int n = beetles.length;
		
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - 1); j++) {
				
				if (beetles[j-1].compareTo(beetles[j]) < 0){
					
					swap = beetles[j-1];
					beetles[j-1] = beetles[j];
					beetles[j] = swap;
					
					
				}
				
			}
		}
		
		
	}

}
