package no.uib.info233.v2016.sorting.beetles;

import no.uib.info233.v2016.sorting.Beetle;

public class AciliusSulcatus implements Beetle {

	private int weight;
	private int age;
	private String classification;
	private String name; 
	
	public AciliusSulcatus(int weight, int age, String classification, String name) {
		this.weight = weight;
		this.age = age;
		this.classification = classification;
		this.name = name;
	}

	@Override
	public int getWeight() {
		 return weight;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public String getClassification() {
		return classification;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int compareTo(Beetle o) {

		if(o.getWeight() > this.weight) return -1;
		if(o.getWeight() < this.weight) return 1;
		
		return 0;
	}



}
